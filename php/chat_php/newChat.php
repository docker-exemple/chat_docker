<?php
  // recuperation des informations sur le nouveau chat
  if (isset($_REQUEST["chatName"])) $chatName = $_REQUEST["chatName"];
  else $chatName = '';
  if (isset($_REQUEST["description"])) $description = $_REQUEST["description"];
  else $description = '';

  if (isset($_REQUEST["chatName"]) && isset($_REQUEST["newChat"])) {
    try {
        $base = new PDO('mysql:host=mysql;dbname=chat;charset=UTF8;', 'root', file_get_contents('/run/secrets/mysql/password'));
    } catch(exception $e) {
        die('Erreur '.$e->getMessage());
    }
    try {
        $base->exec("SET CHARACTER SET utf8");
        $stmt = $base->prepare('INSERT INTO chat (name, description) VALUES (?,?)');
        $base->beginTransaction();
        $stmt->execute([$chatName, $description]);
        $chatId = $base->query("SELECT LAST_INSERT_ID()")->fetchColumn();
        $base->commit();
    } catch(exception $e) {
        die('Erreur '.$e->getMessage());
    }
  } //else die("Erreur le nom du chat n'est pas défini");
?>
<html>
    <head>
        <title>Création d'un nouveau chat</Title>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1>Création d'un nouveau chat</h1>
        <?php if (! empty ($chatId)) {
        ?>
            <p>Le chat <?= $chatName ?> a bien été créé avec l'id <?= $chatId ?></p>
            <hr>
        <?php
        }
        ?>
        <form method="POST" >
            <p>
                <span>Nom du chat : <input type="text" name="chatName" value="<?= $chatName ?>"><br>
                <span>Description : </span>
                <textarea style="vertical-align: top;" rows="6" cols="50" name="description"></textarea>
                <input style="vertical-align: bottom;" name="newChat" type="submit" value="Créer" />
            </p>   
        </form>
        <p>Retour à la <a href="index.html">page d'accueil</a></p>
    </body>
<html>
